public class FindGrade {

	public static void main(String[] args){
    
		if (args.length==1){
			int score = Integer.parseInt(args[0]);
        		
        		if ((score < 0) || (100 < score)) {
            		System.out.println("it is not a valid score");
        		
        		}else if (score >= 90){
	       			System.out.println("A");
        		
        		}else if (score >= 80){
        		    System.out.println("B");
        		
        		}else if (score >= 70){
        		    System.out.println("C");
        		
        		}else if (score >= 60){
        		    System.out.println("D");
        		
        		}else{ 
        		    System.out.println("F");
        		}

		}
	}	
}


